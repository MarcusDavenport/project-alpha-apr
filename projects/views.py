from .models import Project
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, render, redirect
from .forms import ProjectForm


# Create your views here.
@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"projects": projects}
    return render(request, "projects/project_list.html", context)


@login_required
def project_detail_view(request, id):
    project = get_object_or_404(Project, id=id)
    tasks = project.tasks.all()
    return render(
        request,
        "projects/project_detail.html",
        {"project": project, "tasks": tasks},
    )


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    return render(request, "projects/create_project.html", {"form": form})
